package com.lunamezzogiorno.springsecuritycarsapp;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import jakarta.persistence.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import lombok.*;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.*;
import org.springframework.lang.NonNull;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static jakarta.servlet.DispatcherType.ERROR;
import static jakarta.servlet.DispatcherType.FORWARD;

/**
 * 10.12.2023: Create a project based on Spring Boot using:
 * spring-boot-starter-web
 * spring-boot-starter-security
 * spring-boot-starter-data-jpa
 * MySql
 * Lombok.
 * Create the application's security configuration, where:
 * * the GET/api/cars path requires you to have theADMIN or CARS role
 * * the POST/api/cars path requires you to login
 * * paths starting with /api/users/ require authority ROLE_USER_ADMIN
 * * all other paths do not require authentication
 * * possible ways to log in are Basic Authentication and an automatically generated login form
 * * it is possible to log out
 * * the database is available and working properly
 * * CSRF is not generated on paths starting with /api/
 * In addition, there are 3 users whose data is stored in memory:
 * * user admin with the rolesADMIN and CARS
 * * user admin2 with authorityROLE_USER_ADMIN
 * * user admin3 with roleCARS
 * Each user has the password Secret_123.
 * * 8.04.2024: Added swagger for database API queries
 * About Springdoc + swagger: https://springdoc.org/#getting-started
 * Swagger implementation help:
 * https://swagger.io/
 * https://medium.com/@berktorun.dev/swagger-like-a-pro-with-spring-boot-3-and-java-17-49eed0ce1d2f
 * https://www.baeldung.com/spring-rest-openapi-documentation
 * https://medium.com/@javedalikhan50/comprehensive-guide-to-openapi-swagger-integration-in-spring-boot-with-spring-security-jwt-edf8c84e7d91
 * https://stackoverflow.com/questions/66867480/swagger-ui-keeps-showing-example-petstore-instead-of-my-api
 */


@EnableMethodSecurity(securedEnabled = true)
@SpringBootApplication
public class SpringSecurityCarsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityCarsAppApplication.class, args);
	}
}

@Repository
interface CarRepository extends JpaRepository<Car, Long>{
}

@Configuration
class SecurityConfig{

	private static final String[] AUTH_WHITELIST = {
			"/v3/api-docs/**",
			"/swagger-ui/**",
			"/api/auth/**",
			"/api/test/**",
	};

	private final UserDetailsService userDetailsService;

	@Lazy
	public SecurityConfig(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	@Bean
	protected UserDetailsService userDetailsService(PasswordEncoder encoder){
		UserDetails admin = User.withUsername("admin1")
				.password(encoder.encode("Secret_123"))
				.roles("ADMIN", "CARS")
				.build();
		UserDetails admin2 = (User) User.withUsername("admin2")
				.password(encoder.encode("Secret_123"))
				.authorities("ROLE_USER_ADMIN")
				.build();
		UserDetails admin3 = (User) User.withUsername("admin3")
				.password(encoder.encode("Secret_123"))
				.roles("CARS")
				.build();
		return new InMemoryUserDetailsManager(admin, admin2, admin3);
		//return userDetailsService;
	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception{
		return httpSecurity
				.authorizeHttpRequests(auth -> auth
						.dispatcherTypeMatchers(FORWARD, ERROR).permitAll()
						.requestMatchers(AUTH_WHITELIST).permitAll()
						.requestMatchers("/", "/**").permitAll()
						.requestMatchers(HttpMethod.GET, "/api/cars").hasRole("ADMIN")
						.requestMatchers(HttpMethod.GET,"/api/cars").hasRole("CARS")
						.requestMatchers(HttpMethod.POST,"/api/cars").hasAnyRole("ADMIN", "CARS", "USER_ADMIN")
						.requestMatchers(HttpMethod.POST,"/api/users").hasAuthority("ROLE_USER_ADMIN")
						.anyRequest().permitAll())
				.httpBasic(Customizer.withDefaults())
				.formLogin(Customizer.withDefaults())
				.logout(Customizer.withDefaults())
				.csrf((csrf) ->  csrf
						.requireCsrfProtectionMatcher(new AntPathRequestMatcher("/api/**"))
						.disable()
				).build();
	}
}


class SwaggerConfig{
	@Bean
	GroupedOpenApi publicApi(){
		return GroupedOpenApi.builder()
				.group("public-apis")
				.pathsToMatch("/**")
				.build();
	}

	@Bean
	public OpenAPI openAPI() {
		return new OpenAPI()
				.info(new Info()
						.title("Web service API")
						.version("1.0.0"))
				.addSecurityItem(new SecurityRequirement().addList("bearerAuth"))
				.components(
						new Components()
								.addSecuritySchemes("bearerAuth", new SecurityScheme()
										.type(SecurityScheme.Type.HTTP)
										.scheme("bearer")
										.bearerFormat("JWT")));
	}
}

@Service
class DbInitializer implements CommandLineRunner {

	private final CarRepository carRepository;

	public DbInitializer(CarRepository carRepository) {
		this.carRepository = carRepository;
	}

	@Override
	public void run(String... args) {
		saveCars();

	}

	public void saveCars(){
		this.carRepository.deleteAll();

		// Create initial dummy products
		Car c1 = new Car("A5", "Audi");
		Car c2 = new Car("Civic", "Honda");
		Car c3 = new Car("Corolla", "Toyota");

		// Save to db
		this.carRepository.saveAll(Arrays.asList(c1, c2, c3));
	}
}

@Transactional
@AllArgsConstructor
@RestController
@RequestMapping("")
class CarController{
	CarRepository carRepository;

	@GetMapping("/")
	public String getMainPage() {
		return "Testing... Main page";
	}

	@GetMapping("/api")
	public String getSomeOtherPage() {
		return "Testing... This is an API page";
	}

	@Secured("ROLE_ADMIN")
	@GetMapping("/api/cars")
	public ResponseEntity<List<Car>> getCars(){
			var cars = carRepository.findAll();
			System.out.println(cars);
			return new ResponseEntity<>(cars, HttpStatus.OK);
	}

	@Secured("ROLE_USER_ADMIN")
	@GetMapping("/api/users")
	public ResponseEntity<Object> getUsers(Authentication authentication){
		var user = authentication.getDetails();
		System.out.println(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}


	@PostMapping("/api/cars")
	public ResponseEntity<Void> createItem(@RequestBody Car car){
		carRepository.save(car);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}

@AllArgsConstructor
class UserDetailsAdapter implements UserDetails{

	private User user;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return List.of(	new SimpleGrantedAuthority("ROLE_ADMIN"),
						new SimpleGrantedAuthority("ROLE_CARS"),
						new SimpleGrantedAuthority("ROLE_USER_ADMIN")
		);
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}


@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
class Car{
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NonNull private String model;
	@NonNull private String manufacturer;
}
