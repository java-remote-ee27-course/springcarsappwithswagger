# A Spring Boot / Security Cars app with MySQL DB, RestAPI and Swagger

10.12.2023

## Description

A Spring boot / Security / MySQL / RestAPI simple demo program where I later added Swagger for demo purpose.

**A project based on Spring Boot using:**

* spring-boot-starter-web
* spring-boot-starter-security
* spring-boot-starter-data-jpa
* MySql
* Lombok

**It has security configuration, where:**

* the GET/api/cars path requires you to have theADMIN or CARS role
* the POST/api/cars path requires you to login
* paths starting with /api/users/ require authority ROLE_USER_ADMIN
* all other paths do not require authentication
* possible ways to log in are Basic Authentication and an automatically generated login form
* it is possible to log out
* the database is available and working properly
* CSRF is not generated on paths starting with /api/

**In addition, there are 3 users whose data is stored in memory:**

* user admin with the rolesADMIN and CARS
* user admin2 with authorityROLE_USER_ADMIN
* user admin3 with roleCARS
* Each user has the password Secret_123

## Updates

**8.04.2024: Added swagger for database API queries** 

* About Springdoc + swagger: https://springdoc.org/#getting-started
* Swagger implementation help:
* https://swagger.io/
* https://medium.com/@berktorun.dev/swagger-like-a-pro-with-spring-boot-3-and-java-17-49eed0ce1d2f
* https://www.baeldung.com/spring-rest-openapi-documentation
* https://medium.com/@javedalikhan50/comprehensive-guide-to-openapi-swagger-integration-in-spring-boot-with-spring-security-jwt-edf8c84e7d91
* https://stackoverflow.com/questions/66867480/swagger-ui-keeps-showing-example-petstore-instead-of-my-api

## Prerequisites to run the project

* Java
* Maven
* Spring Boot capable IDE or CLI

## Run the project in your computer

Project classes are in one file as it is just a simple demo project.

* `git clone https://gitlab.com/java-remote-ee27-course/springcarsappwithswagger.git`
* main class: `src/main/java/com/lunamezzogiorno/springsecuritycarsapp/SpringSecurityCarsAppApplication.javaSpringSecurityCarsAppApplication`
* copy file `src/main/resources/application.properties.example` contents to `src/main/resources/application.properties` and change:
  * `spring.datasource.url=jdbc:mysql://localhost:3306/<YOUR_MYSQL_DATABASE_SCHEMA_NAME>?useSSL=false&createDatabaseIfNotExist=true` - add your database/schema name
  * `spring.datasource.username=<YOUR_DB_USERNAME>` - add your MySQL database username
  * `spring.datasource.password=<YOUR_DB_PASSWORD>` - add your MySQL database password
* database is auto-generated (if not existing) and tables are re-created each time when you start the app, due to `createDatabaseIfNotExist=true` and `spring.jpa.hibernate.ddl-auto=create`
* run the project in your computer
* enable Lombok annotations

### Swagger

To see the swagger API queries, run the Spring boot project and go to http://localhost:8080/swagger-ui/index.html

* **Swagger API list of queries**
![swagger2.png](pictures%2Fswagger2.png)

* **Swagger API list of queries with the initial custom error page implementation (I removed it later)**
![swagger1.png](pictures%2Fswagger1.png)